# ZPAQ Dockerfile

Dockerfile to build image and run container to use [ZPAQ](https://mattmahoney.net/dc/zpaq.html). Build on Alpine latest version.

## Usage

### Install

Build from source:

    git clone git@gitlab.com:towojcik/zpaq715.git
    docker build -t alpine-zpaq:latest .

### Run

Run the image, mount the present local working directory and execute a zpaq command that pack files in bind folder. First in the local machine you must go to folder that contains files you want to pack, next run command:

#### Create an archive

    docker run --rm -v $(pwd):/data alpine-zpaq:latest add archive.zpaq .

#### Unpack archive

    mkdir restore
    docker run --rm -v $(pwd):/data alpine-zpaq:latest x archive.zpaq -to restore