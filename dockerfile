# Using alpine base image in latest version
FROM alpine:latest

# Install required packages
RUN apk update && \
    apk upgrade && \
    apk --no-cache add libstdc++ &&\
    apk --no-cache add libgcc &&\
    apk --no-cache add build-base perl && \
    apk --no-cache add git

# Clone the repository with zpaq715
RUN git clone https://github.com/zpaq/zpaq.git

# Seting the work directory to zpaq
WORKDIR /zpaq

# Make command
RUN make && \
    make install

# Move ZPAQ binary to /usr/local/bin
RUN mv /zpaq/zpaq /usr/local/bin

# Seting the work directory to main
WORKDIR /

# Cleanup unnecessary packages and files
RUN apk del build-base perl && \
    apk del git && \
    rm -r zpaq

# Seting the work directory to data
WORKDIR /data

# Seting entrypoint to zpaq
ENTRYPOINT [ "zpaq" ]